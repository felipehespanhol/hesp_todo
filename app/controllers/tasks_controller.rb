class TasksController < ApplicationController
  def index
    @tasks = Task.all
    @new_task = Task.new
  end

  def create
    @task = Task.new(params[:task])

    if @task.save
      respond_to do |format|
        format.json { render json: @task.to_json }
        format.html { redirect_to tasks_path  }
      end
    end
  end

end
