# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->
	cleanFields = (form)-> form.find("input").not('[type="submit"]').val("")	
	getTimeRange = (startTime, endTime)->
		timeRange = "#{startTime.slice(11,16)} - #{endTime.slice(11,16)}"
		return timeRange

	Task = Backbone.Model.extend({urlRoot: '/tasks'})
	TaskView = Backbone.View.extend
		tagName: 'li'
		id: 'task'
		className: 'task-li'
		template: _.template("
			<div class='label task-time'><%= time_range %></div>
			<div class='task-title'><%= title %></div>
		")
		render: ()->
			attributes = this.model.toJSON()
			attributes['time_range'] = getTimeRange(attributes['start_time'], attributes['end_time'])
			this.$el.html this.template(attributes)

	TaskList = Backbone.Collection.extend
		model: Task
		url: '/tasks'
	taskList = new TaskList()
	TaskListView = Backbone.View.extend
		tagName: 'ul'
		className: 'task-list sortable'
		initialize: ()->
			this.collection.on('add', this.addOne, this)
			this.collection.on('reset', this.addAll, this)
		addOne: (task)->
			taskView = new TaskView({model: task})
			this.$el.append(taskView.render().el)
		addAll: ()->
			this.collection.forEach(this.addOne, this)
		render: ()->
			this.addAll()
		
	taskList.fetch()
	taskListView = new TaskListView({collection: taskList})
	taskListView.render()
	$('#task-list').append(taskListView.el)
		
	$('#new-task-modal-button').on 'click', ()-> $('#task_day').val($('#selected_date').val())
	$('#new_task').bind 'ajax:success', (evt, data, status, xhr)->
		task = new Task(data)
		taskView = new TaskView({model:task})
		taskView.render()
		cleanFields $('#new_task')
		$('#new-task-modal').modal 'toggle' 
		$('.task-list').prepend(taskView.el)
		
