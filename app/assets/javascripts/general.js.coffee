$ ->
	cleanFields = (form)->
		form.find("input").not('[type="submit"]').val()	

	$('.sortable').sortable()
	$('.datepicker').datepicker
		format: 'dd/mm/yyyy'
		language: 'pt-BR'
	$('.timepicker').timepicker
		showMeridian: false
		
