class Task < ActiveRecord::Base
  attr_accessible :title, :description, :status, :day, :end_time, :start_time

  def time_range
    "#{start_time.strftime('%H:%M')} - #{end_time.strftime('%H:%M')}" if start_time && end_time
  end
end
