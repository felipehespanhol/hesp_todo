class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :title
      t.string :description
      t.string :status
      t.date :day
      t.time :start_time
      t.time :end_time

      t.timestamps
    end
  end
end
